package com.example.stringdialog;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText StringEt;
    Button ConvertBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        ConvertBtn.setOnClickListener(new ConvertBtnClickListner());
    }

    public void init(){
        StringEt = findViewById(R.id.StringEt);
        ConvertBtn = findViewById(R.id.ConvertBtn);
    }

    class ConvertBtnClickListner implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            StringDialog stringDialog = new StringDialog(MainActivity.this, StringEt.getText().toString());
            stringDialog.setOnUpdateListner(new onUpdateListner());
            stringDialog.setTitle("Test");
            stringDialog.show();
        }
    }

    private class onUpdateListner implements StringDialog.OnUpdateListner {

        @Override
        public void onUpdateRecived(StringDialog stringDialog, String finalString) {
            StringEt.setText(finalString);
            stringDialog.dismiss();
        }
    }
}