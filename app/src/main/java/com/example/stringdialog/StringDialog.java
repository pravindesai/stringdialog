package com.example.stringdialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;

public class StringDialog extends Dialog {
    private Context mContext;
    private String mString, reverseString;
    private EditText stringEt;
    private RadioButton upperRadio, lowerRadio, firstLetterUpRaadio;
    private Button updateBtn;
    private CheckBox reverseCheckBox;
    private RadioGroup Radiogroup;
    private OnUpdateListner mOnUpdateListner;

    public interface OnUpdateListner{
        void onUpdateRecived(StringDialog stringDialog, String finalString);
    }
    public void setOnUpdateListner(OnUpdateListner onUpdateListner){
        this.mOnUpdateListner = onUpdateListner;
    }

    public StringDialog(@NonNull Context context, String myString) {
        super(context);
        mContext = context;
        mString = myString;
        setContentView(R.layout.stringdialog);

        init();

        stringEt.setText(myString);
        updateBtn.setOnClickListener(new UpdateButtonListner());
        Radiogroup.setOnCheckedChangeListener(new RadioButtonCheckedListner());
        reverseCheckBox.setOnCheckedChangeListener(new CheckBoxListner());
    }

    private void init() {
        stringEt =              findViewById(R.id.StringEt);
        updateBtn =             findViewById(R.id.updateBtn);
        Radiogroup=             findViewById(R.id.Radiogroup);
        upperRadio =            findViewById(R.id.upperRadio);
        lowerRadio =            findViewById(R.id.lowerRadio);
        firstLetterUpRaadio =   findViewById(R.id.firstLetterUpRaadio);
        reverseCheckBox =       findViewById(R.id.reverseCheckBox);
    }

    class UpdateButtonListner implements View.OnClickListener{
        @Override
        public void onClick(View view) {
            String finalstr = stringEt.getText().toString();
            Toast.makeText(mContext, "Final String"+ finalstr,Toast.LENGTH_LONG).show();

            if(mOnUpdateListner == null)
                return;
            else {
                mOnUpdateListner.onUpdateRecived(StringDialog.this, finalstr);
            }
            //dismiss();

        }
    }

    class RadioButtonCheckedListner implements RadioGroup.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            if(radioGroup.getCheckedRadioButtonId()      == R.id.upperRadio) {
                stringEt.setText(stringEt.getText().toString().toUpperCase());
            }
            else if(radioGroup.getCheckedRadioButtonId() == R.id.lowerRadio) {
                stringEt.setText(stringEt.getText().toString().toLowerCase());
            }
            else if(radioGroup.getCheckedRadioButtonId() == R.id.firstLetterUpRaadio) {
                stringEt.setText(stringEt.getText().toString().substring(0, 1).toUpperCase() + mString.substring(1).toLowerCase());
            }

        }
    }

    class CheckBoxListner implements CompoundButton.OnCheckedChangeListener{

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
            reverseString = new StringBuffer(stringEt.getText().toString()).reverse().toString();
            if(isChecked) {
                stringEt.setText(reverseString);
            }
            else {
                stringEt.setText(mString);
            }

        }
    }

}
